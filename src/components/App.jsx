import React, {Component} from 'react';
import Clock from './Clock';
import { Form, FormControl, Button } from 'react-bootstrap';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        deadline: 'July 01, 2017',
        newDeadline: ''
    }
  }

  changeDeadlines() {
    this.setState({
        deadline: this.state.newDeadline
    });
  }

  render() {
    return(
      <div className="col-sm-5 col-sm-offset-3">
        <div className="panel-group">
          <div className="panel panel-primary">
            <div className="panel-heading"><h1>Count Down Timer To { this.state.deadline }</h1></div>
            <div className="panel-body">
              <Clock deadline={this.state.deadline} />
                <div className="form-group">
                    <Form inline>
                      <div className="col-sm-8">
                          <input type="text" className="form-control" onChange={event => this.setState({newDeadline: event.target.value})} />
                      </div>
                      <div className="col-sm-2">
                          <Button type="button" onClick={() => this.changeDeadlines()} className="btn btn-primary">
                            Submit
                          </Button>
                      </div>
                    </Form>
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
