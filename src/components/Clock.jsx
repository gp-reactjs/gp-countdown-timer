import React, {Component} from 'react';


class Clock extends Component {

    constructor(props) {
      super(props);
      this.state = {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
      }
    }

    // Load the time algo
    componentWillMount() {
      this.getTimeUntil(this.props.deadline);
    }

    componentDidMount(){
      setInterval(() => this.getTimeUntil(this.props.deadline), 1000);
    }

    leading0(num) {
      return num < 10 ? '0' + num : num;
    }

    getTimeUntil(deadline) {
        const times = Date.parse(deadline) - Date.parse(new Date());

        // Calculation
        const second = Math.floor((times/1000) % 60);
        const minute = Math.floor((times/1000/60) % 60);
        const hour = Math.floor(times/(1000*60*60) % 24);
        const day = Math.floor(times/(1000*60*60*24));

        // Update timer state
        this.setState({
          days: day,
          hours: hour,
          minutes: minute,
          seconds: second
        });
    }

    render(){
      return (
        <ul className="pagination" >
          <li><a href="javascript:void(0)">{this.leading0(this.state.days)} Days</a></li>
          <li><a href="javascript:void(0)">{this.leading0(this.state.hours)} Hours</a></li>
          <li><a href="javascript:void(0)">{this.leading0(this.state.minutes)} Minutes</a></li>
          <li><a href="javascript:void(0)">{this.leading0(this.state.seconds)} Seconds</a></li>
        </ul>
      )
    }
}

export default Clock;
